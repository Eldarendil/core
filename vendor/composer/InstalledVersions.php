<?php











namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '6fae291cc8ec69a74d0c1673186b3d8f319378d4',
    'name' => 'zotlabs/hubzilla',
  ),
  'versions' => 
  array (
    'blueimp/jquery-file-upload' => 
    array (
      'pretty_version' => 'v10.31.0',
      'version' => '10.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0740f81829698b84efe17e72501e0f420ea0d611',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.9.2',
      'version' => '0.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dff976c2f3487d42c1db75a3b180e2b9f0e72ce0',
    ),
    'bshaffer/oauth2-server-php' => 
    array (
      'pretty_version' => 'v1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a0c8000d4763b276919e2106f54eddda6bc50fa',
    ),
    'commerceguys/intl' => 
    array (
      'pretty_version' => 'v1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bf0beb12e37ef1a61e0d09dc66cdaa1a23e62e1',
    ),
    'desandro/imagesloaded' => 
    array (
      'pretty_version' => 'v4.1.4',
      'version' => '4.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '67c4e57453120935180c45c6820e7d3fbd2ea1f9',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'league/html-to-markdown' => 
    array (
      'pretty_version' => '4.10.0',
      'version' => '4.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0868ae7a552e809e5cd8f93ba022071640408e88',
    ),
    'lukasreschke/id3parser' => 
    array (
      'pretty_version' => 'v0.0.3',
      'version' => '0.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '62f4de76d4eaa9ea13c66dacc1f22977dace6638',
    ),
    'michelf/php-markdown' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
    ),
    'pear/text_languagedetect' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e253f26cef9a9066f53f200cc3e0684018cb5b5',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.30',
      'version' => '2.0.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '136b9ca7eebef78be14abf90d65c5e57b6bc5d36',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '28a5c4ab2f5111db6a60b2b4ec84057e0f43b9c1',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd4032040a750077205918c86049aa0f43d22947',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.1.1',
      ),
    ),
    'sabre/dav' => 
    array (
      'pretty_version' => '4.1.5',
      'version' => '4.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1afdc77a95efea6ee40c03c45f57c3c0c80ec22',
    ),
    'sabre/event' => 
    array (
      'pretty_version' => '5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c120bec57c17b6251a496efc82b732418b49d50a',
    ),
    'sabre/http' => 
    array (
      'pretty_version' => '5.1.1',
      'version' => '5.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd0aafede6961df6195ce7a8dad49296b0aaee22e',
    ),
    'sabre/uri' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f502edffafea8d746825bd5f0b923a60fd2715ff',
    ),
    'sabre/vobject' => 
    array (
      'pretty_version' => '4.3.5',
      'version' => '4.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8a0a9ae215a8acfb51afc29101c7344670b9c83',
    ),
    'sabre/xml' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3b959f821c19b36952ec4a595edd695c216bfc6',
    ),
    'simplepie/simplepie' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c68e14ca3ac84346b6e6fe3c5eedf725d0f92c6',
    ),
    'smarty/smarty' => 
    array (
      'pretty_version' => 'v3.1.38',
      'version' => '3.1.38.0',
      'aliases' => 
      array (
      ),
      'reference' => '63b3c0aed0f614880cda21a5c08c606e97b489bb',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'twbs/bootstrap' => 
    array (
      'pretty_version' => 'v4.6.0',
      'version' => '4.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ffb0b48e455430f8a5359ed689ad64c1143fac2',
    ),
    'twitter/bootstrap' => 
    array (
      'replaced' => 
      array (
        0 => 'v4.6.0',
      ),
    ),
    'zotlabs/hubzilla' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '6fae291cc8ec69a74d0c1673186b3d8f319378d4',
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
